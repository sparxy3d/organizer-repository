-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2015 at 11:49 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`comment_id` int(11) NOT NULL,
  `fk_event_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `comment` text
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `fk_event_id`, `fk_user_id`, `date`, `comment`) VALUES
(2, 1, 1, '2015-04-14', 'this is a sample comment'),
(3, 1, 1, '2015-04-07', 'hello world'),
(7, 2, 1, '2015-04-21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
`event_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `event_name` varchar(20) NOT NULL,
  `venue` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `attendees` int(11) DEFAULT NULL,
  `max_attendees` int(11) NOT NULL,
  `approved` enum('0','1','','') NOT NULL DEFAULT '0',
  `time` time NOT NULL,
  `category` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `fk_user_id`, `event_name`, `venue`, `date`, `description`, `attendees`, `max_attendees`, `approved`, `time`, `category`) VALUES
(1, 1, 'christmas', 'nsbm', '2015-04-16', 'sample description of the event', 53, 60, '1', '00:00:00', ''),
(2, 1, 'Halloween', 'Nugegoda', '2015-04-30', 'halloween event at NSBM', 3, 100, '1', '00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `eventuser`
--

CREATE TABLE IF NOT EXISTS `eventuser` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventuser`
--

INSERT INTO `eventuser` (`event_id`, `user_id`) VALUES
(1, 1),
(2, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`user_id` int(11) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `job_title` varchar(20) NOT NULL,
  `employer` varchar(20) NOT NULL,
  `location` varchar(15) NOT NULL,
  `dob` date NOT NULL,
  `contact` varchar(10) NOT NULL,
  `email` varchar(20) NOT NULL,
  `activated` enum('0','1','','') NOT NULL DEFAULT '0',
  `user_type` varchar(10) NOT NULL DEFAULT 'user',
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `job_title`, `employer`, `location`, `dob`, `contact`, `email`, `activated`, `user_type`, `password`) VALUES
(1, 'david', 'henry', 'manager', 'Etisalat', 'Negombo', '1992-02-05', '012124521', 'peter@henry.com', '1', 'admin', '12345'),
(2, 'arjun', 'rana', 'engineer', 'pvt', 'galle', '2015-04-14', '12411', 'ar@ra.com', '0', 'user', '74111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`comment_id`), ADD KEY `fk_user_id` (`fk_user_id`), ADD KEY `fk_event_id` (`fk_event_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`event_id`), ADD KEY `fk_user_id` (`fk_user_id`);

--
-- Indexes for table `eventuser`
--
ALTER TABLE `eventuser`
 ADD PRIMARY KEY (`event_id`,`user_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`fk_user_id`) REFERENCES `user` (`user_id`),
ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`fk_event_id`) REFERENCES `events` (`event_id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`fk_user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `eventuser`
--
ALTER TABLE `eventuser`
ADD CONSTRAINT `eventuser_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
ADD CONSTRAINT `eventuser_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
