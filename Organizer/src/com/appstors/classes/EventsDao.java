package com.appstors.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import event.Event;

@Component
public class EventsDao extends JdbcDao{
	public List<Event> getAllEvents() {
		String sql = "SELECT * FROM ems_events ORDER BY 'due_date','due_time'";
		return getJdbcTemplate().query(sql, new EventMapper());
	}
	
	public List<Event> searchEvent(String query) {
		String sql = "SELECT * FROM ems_events WHERE event_name LIKE %"+query+"%";
		return getJdbcTemplate().query(sql, new EventMapper());
	}
	
	private static final class EventMapper implements RowMapper<Event> {

		@Override
		public Event mapRow(ResultSet rs, int row) throws SQLException {
			Event evt = new Event();
			evt.setEid(rs.getInt("event_id"));
			evt.setEname(rs.getString("event_name"));
			evt.setVenue(rs.getString("location"));
			evt.setDesc(rs.getString("description"));
			evt.setDate(rs.getString("due_date"));
			evt.setTime(rs.getString("due_time"));
			evt.setMaxAttend(rs.getInt("max_seats"));
			
			/*
			evt.setEvent_name("asfdasd");
			evt.setLocation("asdaas");
			evt.setDescription("ljanwdlknasdlkan");
			evt.setDue_date("akujshdlas");
			evt.setDue_time("lakndlas");
			evt.setMaxSeats(12);
			*/
			return evt;
		}
		
	}
}