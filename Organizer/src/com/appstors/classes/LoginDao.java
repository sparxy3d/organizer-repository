package com.appstors.classes;

//import java.sql.ResultSet;
//import java.sql.SQLException;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
//import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import user.User;


@Component
public class LoginDao extends JdbcDao {
	
	public User authenticateUser(Login user){
		String sql = "SELECT * FROM user WHERE email = ? AND password = ?";
		User u = getJdbcTemplate().queryForObject(sql, new Object[]{user.getUsername(), user.getPasswd()}, new LoginUserMapper());
		if(u != null){
			return u;
		} else {
			return null;
		}
	}
	
	public boolean registerUser(User user){
		String sql = "INSERT INTO user (first_name, last_name, email, password) VALUES(?, ?, ?, ?)";
		//getJdbcTemplate().queryForRowSet(sql, new Object[]{user.getUid(), user.getLname()});
		int result = getJdbcTemplate().update(sql, user.getFname(), user.getLname(), user.getEmail(), user.getPasswd());
		if(result > 0){
			return true;
		} else {
			return false;
		}
	}
	
	private static final class LoginUserMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int row) throws SQLException {
			User user = new User();
			user.setFname(rs.getString("first_name"));
			user.setLname(rs.getString("last_name"));
			user.setLocation(rs.getString("location"));
			user.setEmail(rs.getString("email"));
			user.setUid(rs.getInt("user_id"));
			user.setJobTitle(rs.getString("job_title"));
			user.setContact(rs.getString("contact"));
			user.setDob(rs.getString("dob"));
			user.setEmployer(rs.getString("employer"));
			user.setAccess(rs.getString("user_type"));
			return user;
		}
		
	}
}
