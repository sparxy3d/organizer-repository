package com.appstors.classes;

public class Login {
	private String username = "";
	private String passwd = "";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	public Login(String username, String passwd){
		setUsername(username);
		setPasswd(passwd);
	}
	
	public Login(){}
}
