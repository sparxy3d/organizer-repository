package com.appstors.organizer;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import user.User;
import user.UserDao;
import user.UserEvents;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;

@Controller
public class AdminController {

	@RequestMapping(value={"/admin", "/admin/"})
	public ModelAndView admin(HttpSession s, Map<String, Object> map){
		ModelAndView model;
		if(s.getAttribute("access") == "admin"){			
			model = new ModelAndView("Admin");
		}else{
			map.put("status", HttpStatus.NOT_FOUND);
			model = new ModelAndView("403");
		}
		
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		
		model.addObject("msg", "Hello World!");
		
		return model;
	}
	
	@RequestMapping(value="/adminacceptuser", method=RequestMethod.GET)
	public ModelAndView acceptuser(@RequestParam(value="email", required=false) String email, HttpSession s, Map<String, Object> map){
		
		ModelAndView model;
		if(s.getAttribute("access") == "admin"){
			model = new ModelAndView("AdminAcceptUser");
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
			
		     UserDao userDao = 
		      (UserDao)context.getBean("userDao");
		     if(email != null){
		    	 userDao.acceptUser(email);
		     }
			List<User> listUsers = userDao.selectUsers("0");
			model.addObject("listUsers", listUsers);
			
		}else{
			map.put("status", HttpStatus.NOT_FOUND);
			model = new ModelAndView("403");
		}
		
		return model;
	}
	
	@RequestMapping(value="/adminremoveuser", method=RequestMethod.GET)
	public ModelAndView removeuser(@RequestParam(value="email", required=false) String email, HttpSession s, Map<String, Object> map){
		
		ModelAndView model;
		if(s.getAttribute("access") == "admin"){
			model = new ModelAndView("AdminRemoveUser");
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
			
		     UserDao userDao = 
		      (UserDao)context.getBean("userDao");
		     if(email != null){
		    	 userDao.removeUser(email);
		     }
			List<User> listUsers = userDao.selectAllUsers();
			model.addObject("listUsers", listUsers);
		}else{
			map.put("status", HttpStatus.NOT_FOUND);
			model = new ModelAndView("403");
		}
		
		
		return model;
	}
	
	@RequestMapping(value="/adminacceptevent", method=RequestMethod.GET)
	public ModelAndView acceptevent(@RequestParam(value="eid", required=false) String eid, HttpSession s, Map<String, Object> map){
		
		ModelAndView model;
		if(s.getAttribute("access") == "admin"){
			model = new ModelAndView("ApproveEvent");
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
			
		     UserDao userDao = 
		      (UserDao)context.getBean("userDao");
		     if(eid != null){
		    	 userDao.approveEvent(eid);
		     }
			List<UserEvents> listEvents = userDao.approveEvents("0");
			model.addObject("listEvents", listEvents);
		}else{
			map.put("status", HttpStatus.NOT_FOUND);
			model = new ModelAndView("403");
		}
		
		
		return model;
	}
	
	@RequestMapping("/adminsettings")
	@ModelAttribute("user")
	public ModelAndView adminsettings(@RequestParam(value="passwd", required=false) String pass, @ModelAttribute("user") User user, BindingResult result, HttpSession s, Map<String, Object> map){
		
		ModelAndView model;
		if(s.getAttribute("access") == "admin"){
			model = new ModelAndView("AdminSettings");
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
			
		     UserDao userDao = 
		      (UserDao)context.getBean("userDao");
		     if(pass != null){
		    	 userDao.changePass(pass);
		     }
		     model.addObject("msg", "Hello World!");
		}else{
			map.put("status", HttpStatus.NOT_FOUND);
			model = new ModelAndView("403");
			model.addObject("msg", "Hello World!");
		}		
		
		return model;
	}
}
