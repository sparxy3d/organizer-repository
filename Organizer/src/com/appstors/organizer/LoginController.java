 package com.appstors.organizer;

//import com.appstors.classes.Login;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import user.User;

import com.appstors.classes.Login;
import com.appstors.classes.LoginDao;

@Controller
@SessionAttributes("user_id")
public class LoginController {
		
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(){		
		ModelAndView model = new ModelAndView("Login");
		model.addObject("login", new Login());
		return model;
	}
	
	@RequestMapping(value = "/login/authenticate", method = RequestMethod.POST)
	public ModelAndView authenticate(@ModelAttribute("login") Login login, BindingResult result, ModelMap m, HttpSession session, HttpServletResponse response){
		ModelAndView model = new ModelAndView("Login");
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("../dispatcher-servlet.xml");
		LoginDao dao = ctx.getBean("loginDao", LoginDao.class);
		
		//User user = dao.authenticateUser(login);
		
		if(dao.authenticateUser(login) != null) {
			User user = dao.authenticateUser(login);
			//model.addObject("user_id", user.getUid());
			//model.addObject("user_name", user.getFname()+" "+user.getLname());
			//model.addObject("access", user.getAccess());
			session.setAttribute("user_id", user.getUid());
			session.setAttribute("user_name", user.getFname()+" "+user.getLname());
			//session.setAttribute("user_name", "mad");
			session.setAttribute("access", user.getAccess());
			return new ModelAndView("redirect:/index");
		} else if(dao.authenticateUser(login) == null) {
			model.addObject("msg", "<div class='alert alert-danger' role='alert'>Login Failed. Invalid username/password.<a href='#' class='alert-link'>Forgot password?</a></div>");
		}
		//model.addObject("msg", "<div class='alert alert-success' role='alert'>Success</div>");
		return model;
	}
	
	@RequestMapping("/logout")
	public ModelAndView logout(HttpSession session, HttpServletRequest request) throws ServletException {
		//session.invalidate();
		session.setAttribute("user_id", null);
		session.setAttribute("user_name", null);
		session.setAttribute("access", null);
		request.logout();
		return new ModelAndView("redirect:/index");
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public ModelAndView signup(){
		ModelAndView model = new ModelAndView("Signup");
		model.addObject("user", new User());
		return model;
	}
	
	@RequestMapping(value = "/signup/feedback", method = RequestMethod.POST)
	@ModelAttribute("user")
	public ModelAndView signupConfirm(@ModelAttribute("user") User user, BindingResult result, ModelMap m){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("../dispatcher-servlet.xml");
		LoginDao dao = ctx.getBean("loginDao", LoginDao.class);
		dao.registerUser(user);
		ModelAndView model = new ModelAndView("Feedback");
		model.addObject("user", new User().getFname());
		return model;
	}
}
