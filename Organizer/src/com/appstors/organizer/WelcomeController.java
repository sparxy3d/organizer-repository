package com.appstors.organizer;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import user.UserDao;
import user.UserEvents;
import event.Event;
import event.EventDao;


@Controller
public class WelcomeController {
	
	@RequestMapping(value = {"/", "/index", "/index#services", "/index#about", "/index#portfolio", "/index#testimonials"})
	public ModelAndView welcome(HttpSession uid){
		ModelAndView model = new ModelAndView("Index");
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		
	      UserDao userDao = 
	      (UserDao)context.getBean("userDao");
	      
		UserDao ud = new UserDao();
		List<Event> listUpEvent = userDao.topSixEvents();
		
		model.addObject("listUpEvent", listUpEvent);
		model.addObject("msg", "Hello World!");
		
		return model;
	}
	
	@RequestMapping("/contact")
	public ModelAndView contact(){
		ModelAndView model = new ModelAndView("Contact");
		model.addObject("msg", "Hello World!");
		
		return model;
	}
	
	@RequestMapping(value = {"/search-result"}, method = RequestMethod.GET)
	public ModelAndView search(@RequestParam("search") String search){
		ModelAndView model = new ModelAndView("SearchList");
		ApplicationContext ctx  = new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		EventDao dao = ctx.getBean("eventDao", EventDao.class);
		
		List<Event> temp = dao.searchEvents(search);
		if(temp != null){
			model.addObject("result", dao.searchEvents(search));
			return model;
		} else {
			model.addObject("result", "Sorry, 0 events found.");
			return model;
		}
	}
	
	@RequestMapping("/403")
	public ModelAndView denied(){
		ModelAndView model = new ModelAndView("403");
		model.addObject("msg", "Hello World!");
		
		return model;
	}
	
	@RequestMapping(value = {"/404", "/*"})
	public ModelAndView notfound(){
		ModelAndView model = new ModelAndView("404");
		model.addObject("msg", "Hello World!");
		
		return model;
	}
}
