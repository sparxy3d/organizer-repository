package com.appstors.organizer;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import user.User;
import user.UserDao;
import user.UserEvents;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Controller
public class UserController {

	@RequestMapping("/profile")
	public ModelAndView profile(HttpSession uid){
		ModelAndView model = new ModelAndView("Profile");
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		
	      UserDao userDao = 
	      (UserDao)context.getBean("userDao");
	      
		UserDao ud = new UserDao();
		if((Integer)uid.getAttribute("user_id") != null){
			User listUser = userDao.selectUser((Integer)uid.getAttribute("user_id"));
			List<UserEvents> listEvents = userDao.viewEvents((Integer)uid.getAttribute("user_id"));
			List<UserEvents> listEvent = userDao.pastEvents((Integer)uid.getAttribute("user_id"));
			List<UserEvents> listUpEvent = userDao.upcomingEvents((Integer)uid.getAttribute("user_id"));
			model.addObject("listUser", listUser);
			model.addObject("listEvents", listEvents);
			model.addObject("listEvent", listEvent);
			model.addObject("listUpEvent", listUpEvent);
		}		
		
		return model;
	}
	
	@RequestMapping("/editprofile")
	public ModelAndView editprofile(HttpSession uid){
		ModelAndView model = new ModelAndView("EditProfile");
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		
	      UserDao userDao = 
	      (UserDao)context.getBean("userDao");
	      if((Integer)uid.getAttribute("user_id") != null){
	    		User listUser = userDao.selectUser((Integer)uid.getAttribute("user_id"));
	    		model.addObject("listUser", listUser);
	    		model.addObject("user", new User());
	      }
	
		return model;
	}
	
	@RequestMapping("/newprofile")
	@ModelAttribute("user")
	public ModelAndView updateUser(@ModelAttribute("user") User user, BindingResult result, ModelMap m,HttpSession uid) {
		
		ModelAndView model = new ModelAndView("Profile");
		
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		UserDao userDao = 
			      (UserDao)context.getBean("userDao");
		if((Integer)uid.getAttribute("user_id") != null){
			userDao.updateUser(user, (Integer)uid.getAttribute("user_id"));
			User listUser = userDao.selectUser((Integer)uid.getAttribute("user_id"));
			List<UserEvents> listEvents = userDao.viewEvents((Integer)uid.getAttribute("user_id"));
			model.addObject("listUser", listUser);
			model.addObject("listEvents", listEvents);
		}
		
		return model;
	}
	
	@RequestMapping(value="/organizerprofile", method=RequestMethod.GET)
	public ModelAndView organizerprofile(@RequestParam("eid") Integer eid, @RequestParam("uid") Integer uid){
		ModelAndView model = new ModelAndView("OrganizerProfile");
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		
	      UserDao userDao = 
	      (UserDao)context.getBean("userDao");
	      
		UserDao ud = new UserDao();
		User listUser = userDao.selectUser(uid);
		UserEvents listEvent = userDao.viewEvent(eid);
		List<UserEvents> listEvents = userDao.viewEvents(uid);
		model.addObject("listUser", listUser);
		model.addObject("listEvent", listEvent);
		model.addObject("listEvents", listEvents);
		
		return model;
	}
	
}
