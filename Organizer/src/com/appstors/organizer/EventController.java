package com.appstors.organizer;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import user.User;
import event.Comment;
import event.Event;
import event.EventDao;

@Controller
public class EventController {
	
	@RequestMapping(value = {"/events"}, method = RequestMethod.GET)
	public ModelAndView events(@RequestParam(value="cat", required=false) String cat){
		ModelAndView model = new ModelAndView("Events");
		ApplicationContext ctx = new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		EventDao dao = (EventDao)ctx.getBean("eventDao");
		
		//return dao.getAllEvents();
		if(cat != null){
			model.addObject("events", dao.filterEvents(cat));
		}else{
			model.addObject("events", dao.getAllEvents());
		}
		
		
		return model;
	}
	
	@RequestMapping(value = "/new-event", method = RequestMethod.GET)
	public ModelAndView getEentForm(){
		ModelAndView model = new ModelAndView("NewEvent");
		model.addObject("event", new Event());
		return model;
	}
	
	@RequestMapping(value = "/new-event/feedback", method = RequestMethod.POST)
	@ModelAttribute("event")
	public ModelAndView newEvent(@ModelAttribute("event") Event event, BindingResult result, ModelMap m, HttpSession session){
		ModelAndView model = new ModelAndView("NewEvent");
		ApplicationContext ctx = new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
		EventDao dao = 
                (EventDao)ctx.getBean("eventDao");
		
		dao.addEvent(session.getAttribute("user_id").toString(), event);
		//model.addObject("msg", "Hello World!");
		
		return model;
	}
	
    @RequestMapping(value="/event", method={RequestMethod.GET,RequestMethod.POST})
    @ModelAttribute("comment")
    public ModelAndView event(@ModelAttribute("comment") Comment comment, BindingResult result, ModelMap m, @RequestParam(value="eid") Integer eid, @RequestParam(value="uid", required=false) Integer uid, HttpSession id){
        ModelAndView model = new ModelAndView("Event");
        ApplicationContext context = 
                 new ClassPathXmlApplicationContext("/dispatcher-servlet.xml");
        
        EventDao eventDao = 
                  (EventDao)context.getBean("eventDao");
        if((Integer)id.getAttribute("user_id") != null){
            eventDao.addComment(eid, (Integer)id.getAttribute("user_id"), comment.getComment());
            
            int count = eventDao.isRegistered(eid, (Integer)id.getAttribute("user_id"));
            if(count > 0){
                model.addObject("register","yes");
            }
            User regUser = eventDao.getUser((Integer)id.getAttribute("user_id"));
            model.addObject("regUser", regUser);
        }
        if(uid != null){
            eventDao.ticketCount(eid);
            eventDao.registerUser(eid, uid);
        }    
       
        List<User> listUser = eventDao.getRegisteredUsers(eid);

        Event listEvent = eventDao.displayEvent(eid);
        List<Comment> listComment = eventDao.getComments(eid);
        model.addObject("listEvent", listEvent);
        model.addObject("listComment", listComment);
        model.addObject("listUser", listUser);

        
        return model;
    }

}
