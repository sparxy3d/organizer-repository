package event;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import event.Event;
import event.EventMapper;

import user.User;
import user.UserMapper;

@Component
public class EventDao {

private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		
	}
	
	public Event displayEvent(int eid){
		String query = "select * from events where event_id = ?";
		Event event = jdbcTemplate.queryForObject(query, new Object[]{eid}, new EventMapper());
		
		return event;
	}
	
	public List<Event> getAllEvents() {
		String sql = "SELECT * FROM events ORDER BY 'due_date','due_time'";
		List<Event> events = jdbcTemplate.query(sql, new EventMapper());
		return events;
	}
	
	public List<Comment> getComments(int eid){
		String query = "select * from comment inner join user on comment.fk_user_id = user.user_id where comment.fk_event_id = ? order by date desc";
		List<Comment> comments = jdbcTemplate.query(query, new Object[]{eid}, new CommentMapper());
		
		return comments;
	}
	
	public void ticketCount(int eid){
		String query = "update events set attendees = attendees+1 where event_id = ?";
		jdbcTemplate.update(query, new Object[]{eid});
	}
	
	public void registerUser(int eid, int uid){
		String query = "insert into eventuser (event_id,user_id) values(?,?)";
		jdbcTemplate.update(query,eid,uid);
	}
	
	public Integer isRegistered(int eid, int uid){
		int count;
		try{
		String query = "select event_id from eventuser where event_id = ? AND user_id = ?";
		count = jdbcTemplate.queryForObject(query, new Object[]{eid,uid}, Integer.class);
		}catch(Exception e){
			return 0;
		}
		return count;
	}
	
	public List<User> getRegisteredUsers(int eid){
		String query = "select * from eventuser inner join user on eventuser.user_id = user.user_id where eventuser.event_id = ?";
		List<User> users = jdbcTemplate.query(query, new Object[]{eid}, new UserMapper());
		return users;
	}
	
	public void addComment(int eid, int uid, String comment){
		Date date = new Date();
		String query = "insert into comment (fk_event_id, fk_user_id,date,comment) values (?,?,?,?)";
		jdbcTemplate.update(query, eid,uid,date,comment);
	}
	
	public User getUser(int uid){
		String query = "select * from user where user_id = ?";
		User user = jdbcTemplate.queryForObject(query, new Object[]{uid}, new UserMapper());
		return user;
	}
	
	public List<Event> filterEvents(String filter) {
		String sql = "SELECT * FROM events WHERE category = ? ORDER BY 'date'";
		return jdbcTemplate.query(sql, new Object[] {filter}, new EventMapper());
	}
	
	public List<Event> searchEvents(String name) {
		String temp = "";
		temp = "%"+name+"%";
		String sql = "SELECT * FROM events WHERE event_name LIKE ? ORDER BY 'date'";
		return jdbcTemplate.query(sql, new Object[] {temp}, new EventMapper());
	}
	
	public void addEvent(String uid, Event evt){
		String query = "insert into events (fk_user_id, event_name, venue, date, description, max_attendees, time, category) values (?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(query, uid, evt.getEname(), evt.getVenue(), evt.getDate(), evt.getDesc(), evt.getMaxAttend(), evt.getTime(), evt.getCategory());
	}
}
