package event;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CommentMapper implements RowMapper<Comment> {

	public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
	      Comment comment = new Comment();
	      comment.setCid(rs.getInt("comment_id"));
	      comment.setUid(rs.getInt("fk_user_id"));
	      comment.setFname(rs.getString("first_name"));
	      comment.setLname(rs.getString("last_name"));
	      comment.setDate(rs.getString("date"));
	      comment.setComment(rs.getString("comment"));
	      return comment;
	   }
}
