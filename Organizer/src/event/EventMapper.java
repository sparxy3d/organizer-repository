package event;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EventMapper implements RowMapper<Event> {

	public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
	      Event event = new Event();
	      event.setEid(rs.getInt("event_id"));
	      event.setUid(rs.getInt("fk_user_id"));
	      event.setEname(rs.getString("event_name"));
	      event.setVenue(rs.getString("venue"));
	      event.setDesc(rs.getString("description"));
	      event.setDate(rs.getString("date"));
	      event.setTime(rs.getString("time"));
	      event.setMaxAttend(rs.getInt("max_attendees"));
	      event.setAttend(rs.getInt("attendees"));
	      event.setCategory(rs.getString("category"));
	      return event;
	   }
}
