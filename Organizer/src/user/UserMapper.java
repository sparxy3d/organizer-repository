package user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User> {
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
	      User user = new User();
	      user.setUid(rs.getInt("user_id"));
	      user.setFname(rs.getString("first_name"));
	      user.setLname(rs.getString("last_name"));
	      user.setJobTitle(rs.getString("job_title"));
	      user.setEmployer(rs.getString("employer"));
	      user.setLocation(rs.getString("location"));
	      user.setContact(rs.getString("contact"));
	      user.setEmail(rs.getString("email"));
	      user.setDob(rs.getString("dob"));
	      return user;
	   }
}