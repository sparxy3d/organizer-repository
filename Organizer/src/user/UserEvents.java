package user;

import java.util.Date;

public class UserEvents {
	private Integer eid;
	private Integer uid;
	private String ename;
	private String venue;
	private String desc;
	private Date date;
	private int maxAttend;
	
	public Integer getEid() {
		return eid;
	}
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getMaxAttend() {
		return maxAttend;
	}
	public void setMaxAttend(int maxAttend) {
		this.maxAttend = maxAttend;
	}

}
