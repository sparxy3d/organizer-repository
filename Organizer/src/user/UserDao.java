package user;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Component;

import event.Event;
import event.EventMapper;

@Component
public class UserDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		
	}
	
	public void updateUser(User u, int uid) {
		String updQuery = "update user set first_name = ? , last_name = ? , job_title = ? , "
				+ "employer = ? , location = ? , email = ?  where user_id = ?";
		/*String updQuery = "update user set first_name = ? where id = ?";*/
		int count = jdbcTemplate.update(updQuery, u.getFname(), u.getLname(), u.getJobTitle(),
				u.getEmployer(), u.getLocation(), u.getEmail(), uid );
		if(count!=0)
			System.out.println("User updated successfully.");
		else
			System.out.println("Couldn't update user with given id as it doesn't exist");
	}
	
	public User selectUser(int uid) {
		String selQuery = "select * from user where user_id = ?";
		User user = jdbcTemplate.queryForObject(selQuery, new Object[]{uid}, new UserMapper());
		
		return user;
	}
	
	public List<User> selectUsers(String id) {
		String selQuery = "select * from user where activated = ?";
		List<User> users = jdbcTemplate.query(selQuery, new Object[]{id}, new UserMapper());
		
		return users;
	}
	
	public List<User> selectAllUsers() {
		String selQuery = "select * from user";
		List<User> users = jdbcTemplate.query(selQuery, new Object[]{}, new UserMapper());
		
		return users;
	}
	
	public UserEvents viewEvent(int eid) {
		String selEventQuery = "select * from events where event_id = ?";
		UserEvents ue = jdbcTemplate.queryForObject(selEventQuery, new Object[]{eid}, new UserEventMapper());
		
		return ue;
	}
	
	public List<UserEvents> viewEvents(int uid) {
		String selEventsQuery = "select * from events where fk_user_id = ? ORDER BY events.date LIMIT 3";
		List<UserEvents> ue = jdbcTemplate.query(selEventsQuery, new Object[]{uid}, new UserEventMapper());
		
		return ue;
	}
	
	public void approveEvent(String eid) {
		String query = "update events set approved = '1' where event_id = ?";
		int count = jdbcTemplate.update(query, eid);		
		if(count!=0)
			System.out.println("Event approved successfully.");
		else
			System.out.println("Couldn't approve event with given id as it doesn't exist");
	}
	
	public List<UserEvents> approveEvents(String eid) {
		String selEventsQuery = "select * from events where approved = ?";
		List<UserEvents> ue = jdbcTemplate.query(selEventsQuery, new Object[]{eid}, new UserEventMapper());
		
		return ue;
	}
	
	public void acceptUser(String mail) {
		String query = "update user set activated = '1' where email = ?";
		int count = jdbcTemplate.update(query, mail);
		if(count!=0)
			System.out.println("User updated successfully.");
		else
			System.out.println("Couldn't update user with given id as it doesn't exist");
	}
	
	public void removeUser(String mail) {
		String query = "delete from user where email = ?";
		int count = jdbcTemplate.update(query, mail);
		if(count!=0)
			System.out.println("User deleted successfully.");
		else
			System.out.println("Couldn't delete user with given id as it doesn't exist");
	}
	
	public void changePass(String pass) {
		String query = "update user set password = ? where user_type='admin'";
		int count = jdbcTemplate.update(query, pass);		
		if(count!=0)
			System.out.println("Admin password changed successfully.");
		else
			System.out.println("Couldn't change password with given id as it doesn't exist");
	}
	
	public List<UserEvents> pastEvents(Integer uid) {
		String selEventsQuery = "select * from events inner join eventuser on events.event_id = eventuser.event_id where eventuser.user_id = ? AND events.date < CURDATE() ORDER BY events.date DESC LIMIT 3";
		List<UserEvents> ue = jdbcTemplate.query(selEventsQuery, new Object[]{uid}, new UserEventMapper());
		
		return ue;
	}
	
	public List<UserEvents> upcomingEvents(Integer uid) {
		String selEventsQuery = "select * from events inner join eventuser on events.event_id = eventuser.event_id where eventuser.user_id = ? AND CURDATE() < events.date ORDER BY events.date LIMIT 3";
		List<UserEvents> ue = jdbcTemplate.query(selEventsQuery, new Object[]{uid}, new UserEventMapper());
		
		return ue;
	}
	
	public List<Event> topSixEvents() {
		String sql = "SELECT * FROM events ORDER BY events.date LIMIT 6";
		List<Event> evt = jdbcTemplate.query(sql, new EventMapper());
		return evt;
	}
}

