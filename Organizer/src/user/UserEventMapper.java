package user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserEventMapper implements RowMapper<UserEvents>{
	public UserEvents mapRow(ResultSet rs, int rowNum) throws SQLException {
	      UserEvents ue = new UserEvents();
	      ue.setEid(rs.getInt("event_id"));
	      ue.setUid(rs.getInt("fk_user_id"));
	      ue.setEname(rs.getString("event_name"));
	      ue.setVenue(rs.getString("venue"));
	      ue.setDesc(rs.getString("description"));
	      ue.setMaxAttend(rs.getInt("max_attendees"));
	      ue.setDate(rs.getDate("date"));
	      return ue;
	   }
}
