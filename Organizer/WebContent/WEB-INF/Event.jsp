<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="head.inc.jsp" />
<body>
<style>
	h2{
		font-weight: bold;
	}
	.dark h3 {
		color: #fff;
	}
	h2, h3{
		margin:15px;
		padding:37px;
		text-align:center;
		color:rgb(246, 66, 66);
	}
	#prof{
		background-color:#fff;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#000;
		font-size:1.2em;
	}
	span{
		font-size:1.3em;
		font-weight:bold;
	}
	span a{
		color:#000;
	}
	span a:hover{
		text-decoration:none;
		color:#D4D4D4;
	}
	.blank{
		margin-top:50px;
	}
	img{
		margin: 10px auto;
	}
	#editprofile{
		border-radius:0px;
		box-shadow: 1px 1px 3px #000;
	}
	#editprofile a{
		color:#000;
	}
	#editprofile a:hover{
		text-decoration:none;
		color:#000;
	}
	.userComments{
		text-align:left;
		background-color:#E0E0E0;
		padding:15px;
		margin-bottom:15px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userComments:nth-child(even){
		background-color:#E3F2FD;
	}
	.userComments p{
		margin-top:10px;
		margin-left:40px;
	}
	.userComments p a:hover{
		text-decoration:none;
		color:#000;
	}
	#ticket,#viewticket{
		text-decoration:none;
		color:#fff;
		background-color:#1565C0;
		padding:10px;
		box-shadow: 1px 1px 3px #000;
	}
	#countdown{
		color:#fff;
		background-color:#f44336;
		padding:5px;
		box-shadow: 1px 1px 3px #000;
	}
	#eventside p{
		color:#fff;
		background-color:#1565C0;
		padding:10px;
		box-shadow: 1px 1px 3px #000;
	}
	#commentpic{
		width:100px;
		height:100px;
		margin:0 10px;
	}
	#eventbody{
		padding:15px;
		color:#fff;
		background-color:#6A1B9A;
		box-shadow: 1px 1px 3px #000;
	}
	#sendComment{
		margin-bottom:15px;
	}
	.regUsers{
		background-color:#7995F1;
		padding-top:15px;
		padding-bottom:15px;
	}
	#orgprofile a:hover{
		text-decoration:none;
	}
	
</style>
	<!--  header -->
	<jsp:include page="header.inc.jsp" />
	
	<div class="content">
		<div class="services-section1" id="services1">
			<div class="container">
				<jsp:include page="nav.inc.jsp" />			
			</div>
		</div>
		
		<div class="container">	
			
			<div class="col-md-12" id="prof">
				<div class="col-md-8 col-md-offset-2">
					<h2>${listEvent.ename} Event Page</h2>				
				</div>
			
				<div class="col-md-8 col-md-offset-2">
					<img src="http://placehold.it/200" class="img-responsive">
				</div>
				
				<div class="col-md-6 col-md-offset-3" id="orgprofile">
					<a class="hvr-shutter-in-vertical" href="organizerprofile?eid=${listEvent.eid}&uid=${listEvent.uid}">Event Organizer Profile</a>
				</div>
				
				<div class="col-sm-12 blank"></div>
			 	<div class="col-md-7 col-md-offset-1" id="eventbody">
					<p><span>${listEvent.ename} </span> is being organized in all its glory at <span>${listEvent.venue}</span><br>
					on <span>${listEvent.date}</span> at <span>${listEvent.time}</span>.<br/>
					Seats are limited, so HURRY UP and make sure you attend!
					</p><br/>
					<p>${listEvent.desc}</p>
				</div>
				<div class="col-md-3 col-md-offset-1" id="eventside">
				<c:if test="${not empty user_id}">
				<c:if test="${listEvent.attend < listEvent.maxAttend}">
					<c:if test="${empty register}">
					<a href="#" id="ticket">GET TICKET</a>
					</c:if>
					<c:if test="${not empty register }">
					<a href="#" id="viewticket">View your Ticket?</a>
					</c:if>
				</c:if>
				</c:if>
				<c:if test="${empty user_id}">
					<p>Unauthorized to get ticket</p>		
				</c:if>
				<c:if test="${listEvent.attend >= listEvent.maxAttend}">
					<p>Tickets have vanished :(</p>
				</c:if>
					<br/><br/>
					<div id="countdown">
						Tickets Countdown <br/>
						<span>${listEvent.attend}</span> / <span>${listEvent.maxAttend}</span>
					</div>
				</div>
				<c:if test="${not empty user_id}">
				<c:set var="ticketvalue" value="Authorized entry pass for + ${listEvent.ename} + on + ${listEvent.date} + Name: ${regUser.fname} ${regUser.lname} + UserID: ${regUser.uid}"/> 
				</c:if>
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
				<h3>Who are Attending this Event</h3><br/>
				<c:forEach items="${listUser}" var="user">
					<div class="col-md-5 regUsers"><img src="resources/images/placeholder3.png" alt="registered Users" style="width:100px;height:100px;" /> ${user.fname} ${user.lname}, ${user.jobTitle}</div>
					<div class="col-md-1"></div>
				</c:forEach>
				</div>
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<h3>Event Discussion</h3>
					</div>
					<div class="col-sm-12 blank"></div>
					<c:if test="${not empty user_id}">
					<div class="col-md-10 col-md-offset-1" id="sendComment">
						<form:form method="post" modelAttribute="comment" action="event?eid=${listEvent.eid}">
							<form:label path="comment">Enter your comment:</form:label>
							<form:textarea path="comment" rows="4"/>
							<input type = "submit" value="Comment" />
						</form:form>
					</div>
					</c:if>
					<c:forEach items="${listComment}" var="comment">					
					<div class="col-md-12 userComments">
						<div class="col-md-2">
							<img alt="Profile Pic" src="resources/images/placeholder1.png" id="commentpic">
						</div>
						<div class="col-md-9">
							<p><span>${comment.fname} ${comment.lname}</span> said on <span style="font-style:italic;">${comment.date}</span> : <br/><br/>
							<span>${comment.comment}</span></p>
						</div>
						
					</div>
					</c:forEach>
				</div>
				
				<div class="col-sm-12 blank"></div>
			</div>
			
		</div>
	</div>
	<jsp:include page="footer.inc.jsp" />
	
	<div id="ticketmodal" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Your Ticket</h4>
	            </div>
	            <div class="modal-body">
	                <img src="https://api.qrserver.com/v1/create-qr-code/?data=${ticketvalue}&amp;size=100x100" alt="" title="" style="display:block;margin-left:auto;margin-right:auto;" />
	            	<p style="text-align:center;">You have got your ticket! Please scan this code with your smartphone</p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-primary" id="sendmail"><a href="event?eid=${listEvent.eid}&uid=${user_id}" style="color:#fff;">Confirm</a></button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- CORE JQUERY SCRIPTS -->
    <script src="resources/js/jquery-1.8.3.min.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="resources/js/bootstrap.min.js"></script>
    <script>
	    $(document).ready(function(){
	        $("#ticket").click(function(){
	            $("#ticketmodal").modal('show');
	            $("#sendmail").show();
	        });
	        $("#viewticket").click(function(){
	            $("#ticketmodal").modal('show');
	            $("#sendmail").hide();
	        });
	       
	    });
    </script>
</body>
</html>