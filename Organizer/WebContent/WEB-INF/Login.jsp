<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="head.inc.jsp" />
<%@page session="true"%>
<body>
<!-- header -->
<div class="content">
	<div class="services-section1" id="services1">
		<div class="container">
		<jsp:include page="nav.inc.jsp" />
	<div class="col-sm-6 col-sm-offset-3" style="margin-top: 98px;">
	
	<style>
    .form{ 
    max-width: 75%; 
    min-width: 20%; 
    border-width: 1px; 
    border-color: #CCCCCC; 
    border-radius: 4px; 
    border-style: solid; 
    color: #222222; 
    font-size: 14px; 
    margin: 0px; 
    background-color: #FCFCFC; 
    padding: 25px; 
} 
.content{ 
    margin: 25px; 
} 
.form label{ 
    color: #222222; 
    font-size: 14px; 
    display: block; 
} 
.form input[type=radio], input[type=checkbox]{ 
    margin: 10px; 
    width: 13px; 
} 
.form div{ 
    display: block; 
} 
.form input, form textarea, form select, form button{ 
    border-width: 1px; 
    border-style: solid; 
    border-color: #EDEDED; 
    border-radius: 1px; 
    padding: 6px; 
    width: 100%; 
} 
.form, .form h1, .form h2{ 
    font-family: 'Trebuchet MS'; 
} 
.form h1{ 
    font-size: 30px; 
    color: #FF264A; 
    padding: 0px; 
    margin: 0px; 
    margin-bottom: 0px; 
    border-bottom-style: dotted; 
    border-bottom-color: #999999; 
    border-bottom-width: 2px; 
    border-radius: 0px; 
    background-color: #FAFAFA; 
} 
.intro{ 
    margin-bottom: 1px; 
} 
.clear{ 
    clear: both; 
} 
.form textarea{ 
    height: 60px; 
    width: 100%; 
} 
.form button[type=submit]{ 
    width: 100%; 
    background-color: #F55151; 
    color: #FFFFFF; 
} 
.field{ 
    margin-bottom: 5px; 
} 
</style>

	<form:form modelAttribute="login" id="form" class="form" method="post" action="login/authenticate">
    <h1>Log in</h1>
    <div class="content">
        <div class="intro"><h4>Not registered with us yet? <span style="text-decoration: underline;"><a href="signup">Sign up</a></span></h4></div>
        <br /><br />
        <div id="section0" >
            <div class="field">
            <form:label path="username">Username</form:label>
            <form:input id="username" path="username" size="50" required="required" autofocus="autofocus" />
            </div>
            <div class="field">
            <form:label path="passwd">Password</form:label>
            <form:password id="passwd" path="passwd" required="required" />
            </div>
            <div class="field">
            <button type="submit" id="login" name="login">Log in</button>
            </div>
        </div>
    </div>
	</form:form>
	</div>
		</div>
	</div>
</div>
<jsp:include page="footer.inc.jsp" />
</body>
</html>