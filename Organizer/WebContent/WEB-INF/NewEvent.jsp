<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="head.inc.jsp" />
<%@page session="true"%>
<body>
<!-- header -->

<link href="<c:url value="/resources/css/new-event-form.css"/>" rel="stylesheet" type="text/css" media="all" />
<div class="content">
	<div class="services-section1" id="services1">
		<div class="container">
		<jsp:include page="nav.inc.jsp" />
<div class="col-sm-6 col-sm-offset-3" style="margin-top:85px;">
<c:if test="${empty session.getAttribute('user_name')}">
<form:form id="form" class="form" modelAttribute="event" method="post" action="new-event/feedback">
    <h1>Create New Event</h1>
    <div class="content">
        <div class="intro"><h5 style="text-align: center;"><em>The Organizer - Create your own event</em></h5></div>
        <div id="section0" >
            <div class="field">
            	<form:label path="ename">Event Name</form:label>
            	<form:input id="eventName" path="ename" maxlength="50" placeholder="ex : Birthday Party" required="required" autofocus="autofocus"></form:input>
            </div>
            <div class="field">
            	<form:label path="venue">Location</form:label>
            	<form:input id="location" path="venue" maxlength="50" placeholder="Add a place" required="required"></form:input>
            </div>
            <div class="field">
            	<form:label path="desc">Description</form:label>
            	<form:textarea id="description" path="desc" rows="4" placeholder="Add more information" wrap="hard" required="required"></form:textarea>
            </div>
            <div class="field">
            	<form:label path="date">Date</form:label>
            	<form:input id="date" path="date" placeholder="Add a date" required="required"></form:input>
            </div>
            <div class="field">
            	<form:label path="time">Time</form:label>
            	<form:input id="time" path="time" placeholder="Add a time" required="required"></form:input>
            </div>
            <div class="field">
            	<label path="category">Category</label>
            	<form:select id="category" path="category" required="required">
            		<form:option value="1">Entertainment</form:option>
            		<form:option value="2">Technology</form:option>
            		<form:option value="3">Education</form:option>
            		<form:option value="4">Sports</form:option>
            		<form:option value="5">Religion</form:option>
            		<form:option value="6">Health</form:option>
            		<form:option value="7">Business</form:option>
            		<form:option value="8">Lifestyle</form:option>
            		<form:option value="9">Other</form:option>
            	</form:select>
            </div>
            <div class="field">
            	<form:label path="maxAttend">Seats Available</form:label>
            	<form:input id="maxAttend" path="maxAttend" placeholder="Set maximum attenders" required="required"></form:input>
            </div>
            <div class="field">
            	<button type="submit" id="create" name="submit">Create Event</button>
            </div>
            <div class="field">
            	<button type="reset">Cancel</button>
            </div>
        </div>
    </div>
</form:form>
</c:if>
<c:if test="${not empty session.getAttribute('user_name')}">
<h3>Sorry, You have to login to create and publish an event.</h3>
</c:if>
<script type="text/javascript">
$(function(){
	$('#date').datepicker({
		dateFormat: "yy-mm-dd"
	});
	$('#time').timepicker({ 'timeFormat': 'H:i:s' });
	
	
})
</script>
	</div>
		</div>
	</div>
</div>
<jsp:include page="footer.inc.jsp" />
</body>
</html>