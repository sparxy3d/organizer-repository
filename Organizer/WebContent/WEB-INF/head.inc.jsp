<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE HTML >
<html>
<head>
<title>Organizer</title>

<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" type="text/css" media="all">
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value="/resources/css/jquery-ui.css"/>" rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value="/resources/css/jquery.timepicker.css"/>" rel="stylesheet" type="text/css" media="all" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<script src="<c:url value="/resources/js/jquery-1.8.3.min.js"/>" ></script>
<script src="<c:url value="/resources/js/jquery-ui.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.timepicker.min.js"/>"></script>
<script src="<c:url value="/resources/js/classie.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mixitup.min.js"/>"></script>
<!---- start-smooth-scrolling---->
<script type="text/javascript" src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/easing.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
 <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
<!---End-smooth-scrolling----
	 <!------ Light Box ------>
    <link rel="stylesheet" href="<c:url value="/resources/css/swipebox.css"/>">
    <script src="<c:url value="/resources/js/jquery.swipebox.min.js"/>"></script> 
    <script type="text/javascript">
		jQuery(function($) {
			$(".swipebox").swipebox();
		});
	</script>
    <!------ Eng Light Box ------>	
    
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/component.css"/>" />
		<script src="<c:url value="/resources/js/modernizr.custom.js"/>"></script>
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-7243260-2']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>

</head>