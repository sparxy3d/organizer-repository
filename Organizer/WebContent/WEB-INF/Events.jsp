<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="head.inc.jsp" />
<body>
<!-- header -->
		<jsp:include page="header.inc.jsp" />
				<div class="content">
					<div class="services-section1" id="services1">
					<div class="services-header">
							<h1>Organizer</h1>
							</div>
							<div class="col-sm-12">
								<div class="col-sm-1 col-sm-offset-1"><a href="events">All Events</a></div>
								<div class="col-sm-1"><a href="events?cat=Entertainment">Entertainment</a></div>
								<div class="col-sm-1"><a href="events?cat=Technology">Technology</a></div>
								<div class="col-sm-1"><a href="events?cat=Education">Education</a></div>
								<div class="col-sm-1"><a href="events?cat=Sports">Sports</a></div>
								<div class="col-sm-1"><a href="events?cat=Religion">Religion</a></div>
								<div class="col-sm-1"><a href="events?cat=Health">Health</a></div>
								<div class="col-sm-1"><a href="events?cat=Business">Business</a></div>
								<div class="col-sm-1"><a href="events?cat=Lifestyle">Lifestyle</a></div>
								<div class="col-sm-1"><a href="events?cat=Other">Other</a></div>
							</div>
						<div class="container">
							<jsp:include page="nav.inc.jsp" />
							<div class="col-sm-12">
							<c:forEach items="${events}" var="event">
							<div class="col-sm-4">
								<div class="thumbnail">
									<img src="resources/images/pic5.jpg" class="img-responsive" title="${event.ename}" />
									<div class="caption text-center">
										<h3>${event.ename}</h3>
										<p>On ${event.date}, at ${event.venue}</p>
										<p><a href="event?eid=${event.eid}" class="btn btn-danger" style="background-color: #9522A7; border-color:#9522A7;"><img src="resources/images/leftarrow.png"></a></p>
									</div>
								</div>
							</div>
							
							</c:forEach>
							</div>
					</div>
					</div>
			
					
					<jsp:include page="footer.inc.jsp" />


</body>
</html>