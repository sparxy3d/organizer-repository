<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="head.inc.jsp" />
<body>
<!-- header -->
		<jsp:include page="header.inc.jsp" />
				<div class="content">
					<div class="services-section1" id="services1">
					<div class="services-header">
							<h1 class="cat-header">All Events</h1>
							</div>
							<div class="col-sm-12 cat-list">
								<div class="col-sm-1 col-sm-offset-1"><a href="search-result">All Events</a></div>
								<div class="col-sm-1"><a href="search-result?search=Entertainment">Entertainment</a></div>
								<div class="col-sm-1"><a href="search-result?search=Technology">Technology</a></div>
								<div class="col-sm-1"><a href="search-result?search=Education">Education</a></div>
								<div class="col-sm-1"><a href="search-result?search=Sports">Sports</a></div>
								<div class="col-sm-1"><a href="search-result?search=Religion">Religion</a></div>
								<div class="col-sm-1"><a href="search-result?search=Health">Health</a></div>
								<div class="col-sm-1"><a href="search-result?search=Business">Business</a></div>
								<div class="col-sm-1"><a href="search-result?search=Lifestyle">Lifestyle</a></div>
								<div class="col-sm-1"><a href="search-result?search=Other">Other</a></div>
							</div>
							<script>
							$(document).ready(function(){
								$('.cat-list div a').click(function(){
									var category = $(this).html();
									//console.log(category);
									$('.cat-header').html(category);
								})
							})
							</script>
							<br />
							<br />
						<div class="container">
							<jsp:include page="nav.inc.jsp" />
							<div class="col-sm-12">
							<c:forEach items="${result}" var="event">
							<div class="col-sm-4">
								<div class="thumbnail">
									<img src="resources/images/pic5.jpg" class="img-responsive" title="${event.ename}" />
									<div class="caption text-center">
										<h3>${event.ename}</h3>
										<p>On ${event.date}, at ${event.venue}</p>
										<p><a href="event/?eid=${event.eid}" class="btn btn-danger" style="background-color: #9522A7; border-color:#9522A7;"><img src="resources/images/leftarrow.png"></a></p>
									</div>
								</div>
							</div>
							</c:forEach>
							</div>
					</div>
					</div>
			
					<jsp:include page="footer.inc.jsp" />


</body>
</html>