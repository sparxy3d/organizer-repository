<div class="header" id="home">
	<div class="container">
		<div class="logo">
			<a href="index"><img src="resources/images/logo.png" /></a>
		</div>
		<div class="col-xs-6 col-xs-offset-3 search-con">
			<form action="search-result" class="search" method="GET" modelAttribute="search">
				<input type="text" class="form-control search" name="search" placeholder="Search for your event..."/>
				<style>
				.search-dropdown{
					  position: absolute;
					  border-radius: 0px;
					  margin-top: 5px;
					  margin-left: 15px;
					  max-width: 90%;
					  padding: 10px 5px;
					  display: none;
				}
				.search-dropdown div div {
					padding: 5px 15px;
				}
				.search-dropdown div div:hover {
					background-color: rgb(149, 32, 169);
				}
				.search-dropdown div div:hover a {
					color: #ccc;
				}
				.search-dropdown a {
					color: #888;
					text-decoration: none;
				}
				</style>
				<script>
				$(document).ready(function(){
					$('.search-dropdown a').click(function(){
						var log = $(this).html();
						$('.search-dropdown').hide();
						//console.log(log);
					})
					$('.search').focus(function(){
						$('.search-dropdown').show();
					})
				})
				</script>
				<div class="col-xs-12 well search-dropdown">
					<div class="col-xs-4">
						<div class="col-xs-12"><a href="events">All Events</a></div>
						<div class="col-xs-12"><a href="events?cat=Education">Education</a></div>
						<div class="col-xs-12"><a href="events?cat=Health">Health</a></div>
						<div class="col-xs-12"><a href="events?cat=Other">Other</a></div>
					</div>
					<div class="col-xs-4">
						<div class="col-xs-12"><a href="events?cat=Entertainment">Entertainment</a></div>
						<div class="col-xs-12"><a href="events?cat=Sports">Sports</a></div>
						<div class="col-xs-12"><a href="events?cat=Business">Business</a></div>
					</div>
					<div class="col-xs-4">
						<div class="col-xs-12"><a href="events?cat=Technology">Technology</a></div>
						<div class="col-xs-12"><a href="events?cat=Religion">Religion</a></div>
						<div class="col-xs-12"><a href="events?cat=Lifestyle">Lifestyle</a></div>
					</div>
				</div>
				<button type="submit">&#10140;</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
var url = window.location.pathname;
url = url.substring(url.lastIndexOf('/')+1);
//alert(url);

if(url == 'index') {
	$('.header').css('background', 'url("resources/images/banner.jpg") no-repeat');
	$('.header').css('background-size', 'cover');
} else if(url == 'contact') {
	$('.header').css('background', 'url("resources/images/banner-con.jpg") no-repeat');
	$('.header').css('background-size', 'cover');
} else if(url == 'events') {
	$('.header').css('background', 'url("resources/images/banner-evt.jpg") no-repeat');
	$('.header').css('background-size', 'cover');
} else {
	$('.header').css('background', 'url("resources/images/banner.jpg") no-repeat');
	$('.header').css('background-size', 'cover');
}
</script>