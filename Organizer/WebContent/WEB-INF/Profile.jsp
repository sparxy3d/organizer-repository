<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="head.inc.jsp" />
<body>
<style>
	h2{
		background-color:orange;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#fff;
		
		box-shadow: 1px 1px 3px #000;
	}
	h3{
		background-color:orange;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#fff;
		
		box-shadow: 1px 1px 3px #000;
	}
	#prof{
		background-color:#fff;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#000;
		font-size:1.2em;
	}
	span{
		font-size:1.3em;
		font-weight:bold;
	}
	span a{
		color:#000;
	}
	span a:hover{
		text-decoration:none;
		color:#D4D4D4;
	}
	.blank{
		margin-top:50px;
	}
	#profilepic{
		margin: 10px auto;
		width:250px;
		height:250px;
	}
	#editprofile{
		border-radius:0px;
		box-shadow: 1px 1px 3px #000;
	}
	#editprofile a{
		color:#fff;
	}
	#editprofile a:hover{
		text-decoration:none;
		color:#fff;
	}
	.userEvents{
		background-color:#9C27B0;
		padding:15px;
		margin-bottom:15px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents p a{
		background-color:orange;
		color:#fff;
		padding:5px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents p a:hover{
		text-decoration:none;
		color:#fff;
	}
	
</style>
	<!--  header -->
	<jsp:include page="header.inc.jsp" />
	
	<div class="content">
		<div class="services-section1" id="services1">
			<div class="container">
				<jsp:include page="nav.inc.jsp" />			
			</div>
		</div>
		
		<div class="container">	
			<c:if test="${not empty user_id}">
			<div class="col-md-12" id="prof">
				<div class="col-md-8 col-md-offset-2">
					<h2>${listUser.fname} ${listUser.lname}'s Profile</h2>				
				</div>
			
				<div class="col-md-8 col-md-offset-2">
					<img src="resources/images/placeholder2.png" class="img-responsive" id="profilepic">
				</div>
				
				<div class="col-md-6 col-md-offset-3 buttons">
					<a class="hvr-shutter-in-vertical" id="editprofile" href="editprofile">Edit Profile</a>
				</div>
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<p>Hey. I'm <span>${listUser.fname} ${listUser.lname}</span> hailing from <span>${listUser.location}</span>.<br>
					I am a <span>${listUser.jobTitle }</span> at <span>${listUser.employer}</span>.<br/>
					You can get in touch with me <span><a href="mailto:${listUser.email}">here</a></span>. Cheers!!
					</p>
				</div>	
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<h3>Upcoming Events</h3>
					</div>
					<div class="col-sm-12 blank"></div>
					<c:forEach items="${listUpEvent}" var="uevent">					
					<div class="col-md-8 col-md-offset-2 userEvents">
						<p><span>${uevent.ename}</span> at <span>${uevent.venue}</span> on <span>${uevent.date}</span></p><br/>
						<p><a href="event?eid=${uevent.eid}">More details</a></p>
					</div>
					</c:forEach>
				</div>
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<h3>Organized Events</h3>
					</div>
					<div class="col-sm-12 blank"></div>
					<c:forEach items="${listEvents}" var="event">					
					<div class="col-md-8 col-md-offset-2 userEvents">
						<p><span>${event.ename}</span> at <span>${event.venue}</span> on <span>${event.date}</span></p><br/>
						<p><a href="event?eid=${event.eid}">More details</a></p>
					</div>
					</c:forEach>
				</div>
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<h3>Past Visited Events</h3>
					</div>
					<div class="col-sm-12 blank"></div>
					<c:forEach items="${listEvent}" var="ev">					
					<div class="col-md-8 col-md-offset-2 userEvents">
						<p><span>${ev.ename}</span> at <span>${ev.venue}</span> on <span>${ev.date}</span></p><br/>
						<p><a href="event?eid=${ev.eid}">More details</a></p>
					</div>
					</c:forEach>
				</div>
				<div class="col-sm-12 blank"></div>
			</div>
			</c:if>
			<c:if test="${empty user_id}">
				<div class="col-md-12">
					<h2>Not authorized to view this page</h2>
				</div>
			</c:if>
		</div>
		
	</div>
	<jsp:include page="footer.inc.jsp" />
	
</body>
</html>