<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="head.inc.jsp" />
<body>
<style>
	h2{
		font-weight: bold;
	}
	.dark h3 {
		color: #fff;
	}
	h2, h3{
		margin:15px;
		padding:37px;
		text-align:center;
		color:rgb(246, 66, 66);
	}
	#prof{
		text-align:center;
		font-size: 1.2em;
	}
	span{
		font-size:1.3em;
		font-weight:bold;
	}
	span a{
		color:#fff;
	}
	span a:hover{
		text-decoration:none;
		color:#D4D4D4;
	}
	.blank{
		margin-top:50px;
	}
	#profilepic{
		margin: 10px auto;
		width:250px;
		height:250px;
	}
	#editprofile{
		border-radius:0px;
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents{
		background-color:#9C27B0;
		padding:15px;
		margin-bottom:15px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents p a{
		background-color:orange;
		color:#fff;
		padding:5px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents p a:hover{
		text-decoration:none;
		color:#fff;
	}
	input{
		color:#000;
	}
	button[type=submit]{
		padding:5px 10px;
	}
	input[type=file]{
		margin: 0 auto;
		border: none;
	}
	p input[type=text] {
		border-radius: 50px;
		border: thin solid #F94242;
		text-align: center;
	}
	.hvr-shutter-in-vertical{
		background: rgba(255, 255, 255, 0.23);
	}
	.hvr-shutter-in-vertical:hover{
		box-shadow: none;
	}
	
</style>
	<!--  header -->
	<jsp:include page="header.inc.jsp" />
	
	<div class="content">
		<div class="services-section1" id="services1">
			<div class="container">
				<jsp:include page="nav.inc.jsp" />			
			</div>
		</div>
		
		<div class="container">	
			<c:if test="${not empty user_id}">
			<div class="col-md-12" id="prof">
				<div class="col-md-8 col-md-offset-2">
					<h2>${listUser.fname} ${listUser.lname}'s Profile</h2>				
				</div>
				<form:form method="post" modelAttribute="user" action="newprofile">
				<div class="col-md-8 col-md-offset-2">
					<img src="resources/images/placeholder2.png" class="img-responsive" id="profilepic">	
					<input type="file" name="file"/>				
				</div>
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
				
					<p>Hey. I'm <form:input path="fname" value="${listUser.fname}" /> <form:input path="lname" value="${listUser.lname}" /> hailing from <form:input path="location" value="${listUser.location}" />.<br/><br>
					I am a <form:input path="jobTitle" value="${listUser.jobTitle}" /> at <form:input path="employer" value="${listUser.employer}" size="50"/>.<br/><br/>
					You can get in touch with me <form:input path="email" value="${listUser.email}" size="40" />. Cheers!!
					</p><br/>
					<button type="submit" class="hvr-shutter-in-vertical" style="width:auto;">Update</button>
				</form:form>
				</div>
				</c:if>
				<c:if test="${empty user_id}">
					<div class="col-md-12">
						<h2>Not authorized to view this page</h2>
					</div>
				</c:if>
				<div class="col-sm-12 blank"></div>
			</div>
			
		</div>
		
	</div>
	<jsp:include page="footer.inc.jsp" />
	
</body>
</html>