<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="head.inc.jsp" />
<body>
<!-- header -->
		<jsp:include page="header.inc.jsp" />
		<div class="content">
			<div class="services-section" id="services">
				<div class="container">
					<jsp:include page="nav.inc.jsp" />
					
			<div class="services-header">
							<h1>Organizer</h1>
								<p>Explore the freedom of event management</p>
							</div>
								<div class="services-grids">
									<div class="col-md-4 services-grid">
											<img src="resources/images/icon1.png">
												<h3>Vision</h3>
													<p>Bring freedom to event organizers and attendees.</p>
												</div>
												<div class="col-md-4 services-grid">
													<img src="resources/images/icon2.png">
														<h3>Mission</h3>
															<p>Standardize the world of events.</p>
												</div>
												<div class="col-md-4 services-grid">
													<img src="resources/images/icon3.png">
														<h3>Goal</h3>
														<p>Platform to open up prospects for future development.</p>
												</div>
												<div class="clearfix"></div>
										</div>
										</div>
										</div>
										<div class="about-section" id="about">
											<div class="container">
													<div class="about-grids">
																<div class="col-md-6 left-grid">
																		<img src="resources/images/com.png">
																	</div>
																	<div class="col-md-6 right-grid">
																		<h4>Event management and organizing is not only for the Pros.</h4>
																			<p>Organizer is beginner friendly, so Don't hesitate to get a taste of what is to come in the grand prospective world of Events!<br/><br/>
																			Simplicity is the only the starting of our event encyclopaedia. </p>
																	</div>
															
														</div>
												</div>
										</div>
									
										<div class="portfolio-section" id="portfolio">
											<div class="container">
													<div class="portfolio-header">
														<h3>Upcoming Events</h3>
															<p>Browse through our event lineup. Happiness is where the participation is!</p>
															</div>
															<div id="portfoliolist" class="col-sm-12">
					<c:forEach items="${listUpEvent}" var="event">
							<div class="col-sm-4">
								<div class="thumbnail">
									<img src="resources/images/pic5.jpg" class="img-responsive" title="${event.ename}" />
									<div class="caption text-center">
										<h3>${event.ename}</h3>
										<p><a href="event?eid=${event.eid}" class="btn btn-danger" style="background-color: #9522A7; border-color:#9522A7;"><img src="resources/images/leftarrow.png"></a></p>
									</div>
								</div>
							</div>
							
							</c:forEach>
				<div class="clearfix"></div>					
				</div>
 <!-- Script for gallery Here -->
				
					<script type="text/javascript">
					$(function () {
						
						var filterList = {
						
							init: function () {
							
								// MixItUp plugin
								// http://mixitup.io
								$('#portfoliolist').mixitup({
									targetSelector: '.portfolio',
									filterSelector: '.filter',
									effects: ['fade'],
									easing: 'snap',
									// call the hover effect
									onMixEnd: filterList.hoverEffect()
								});				
							
							},
							
							hoverEffect: function () {
							
								// Simple parallax effect
								$('#portfoliolist .portfolio').hover(
									function () {
										$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
										$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
									},
									function () {
										$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
										$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
									}		
								);				
							
							}
				
						};
						
						// Run the show!
						filterList.init();
						
						
					});	
					</script>
			<!-- Gallery Script Ends -->
			
		
	  
	  <div class="arrow">
				<a href="events"><img src="resources/images/arrow.png"/></a>
				</div>
		</div>
		</div>
				<div class="testimonials-section" id="testimonials">
						<div class="container">
							<div class="test-header">
								<h4>testimonials</h4>
									<p>Feel the experience of our euphoric users</p>
							</div>
							<div class="test-center">
							<p>Being tasked with managing my company anniversary event, I was lost. I didn't know where to begin. Thats where Organizer came to my rescue. I strongly recommend Organizer to anyone who needs to successfully manage their events</p>
								<h4>by</h4>
									<h5>Kunal Perera</h5>
							</div>
						</div>	
					</div>
					<jsp:include page="footer.inc.jsp" />


</body>
</html>