<div class="cbp-spmenu-push">
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s2">
		<div class="logo1">
					<a href="index"><img src="resources/images/logo1.png" /></a>
					</div>
					<ul>
								<li><a href="index">Home</a></li>
							   	<li><a href="index#services" class="scroll">Services</a></li>
							   	<li><a href="index#about" class="scroll">About</a></li> 
							 	<li><a href="index#portfolio" class="scroll">Events</a></li>
							  	<li><a href="index#testimonials" class="scroll">Testimonials</a></li>
								<li><a href="contact">Contact</a></li>
							</ul>
						</nav>
					
		<div class="main">
				<section>
					<button id="showRight"><img src="resources/images/nav.png"></button>
				</section>
		</div>
		
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2-left">
					<ul>
					<% if(session.getAttribute("user_name") == null){ %>
						<li><a href="login">Log in</a></li>
						<li><a href="signup">Sign up</a></li>
						<% } else { %>
						<li><a href="profile"><%= session.getAttribute("user_name") %></a></li>
						<li><a href="logout">Log out</a></li>
						<% } %>
					</ul>
						</nav>
					
		<div class="main">
				<section>
					<button id="showLeft"><img style="border-radius:50%;" src="resources/images/nav-user.png"></button>
				</section>
		</div>
		<div class="main">
				<section>
					<a class="new-event" href="new-event">
						<img style="border-radius:50%;" src="resources/images/nav-event.png">
					</a>
				</section>
		</div>
	</div>
	
		<script>
			var menuRight = document.getElementById( 'cbp-spmenu-s2' ),
				showRight = document.getElementById( 'showRight' ),
				menuLeft = document.getElementById( 'cbp-spmenu-s2-left' ),
				showLeft = document.getElementById( 'showLeft' ),
				body = document.body;

			showRight.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuRight, 'cbp-spmenu-open' );
				disableOther( 'showRight' );
			};
			showLeft.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeft' );
			};

			function disableOther( button ) {
				if( button !== 'showRight' || button !== 'showLeft') {
					classie.toggle( showRight, 'disabled' );
					classie.toggle( showLeft, 'disabled' );
				}
			}
			
			var url = window.location.pathname;
			url = url.substring(url.lastIndexOf('/')+1);
			//alert(url);
			if(url == 'login' || url == 'signup'){
				$('.main #showLeft').hide();
				$('.main .new-event').hide();
			}
		</script>