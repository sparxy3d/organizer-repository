<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Admin Panel - Organizer</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="resources/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="resources/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="resources/css/admin.css" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
        <a class="navbar-brand" href="admin">

                    <h2 style="color:#fff; width:300px; margin-top: -20px;">Organizer Admin</h2>
                </a>
                
					<a href="logout" class="pull-right" id="logout">Logout</a>
        </div>
    </div>

    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Dashboard</h4>
                </div>

            </div>

            <div class="row">
                 <div class="col-md-3 col-sm-3 col-xs-6">
                    <a href="adminacceptuser"><div class="dashboard-div-wrapper bk-clr-one">
                        <i  class="fa fa-user-plus dashboard-div-icon" ></i>
                        <div class="progress progress-striped active"></div>
                         <h5>Accept New Users </h5>
                    </div></a>
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                    <a href="adminremoveuser"><div class="dashboard-div-wrapper bk-clr-two">
                        <i  class="fa fa-user-times dashboard-div-icon" ></i>
                        <div class="progress progress-striped active"></div>
                         <h5>Remove Users </h5>
                    </div></a>
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                    <a href="adminacceptevent"><div class="dashboard-div-wrapper bk-clr-three">
                        <i  class="fa fa-birthday-cake dashboard-div-icon" ></i>
                        <div class="progress progress-striped active"></div>
                         <h5>Approve Events </h5>
                    </div></a>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a href="adminsettings"><div class="dashboard-div-wrapper bk-clr-four">
                        <i  class="fa fa-cogs dashboard-div-icon" ></i>
                        <div class="progress progress-striped active"></div>
                         <h5>Settings </h5>
                    </div></a>
                </div>
            </div>
                <div class="col-md-12">
                    <h4 class="page-head-line"></h4>
                </div>
                </div>
            </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2015 Organizer | Event Management Platform
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="resources/js/jquery-1.8.3.min.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="resources/js/bootstrap.min.js"></script>
</body>
</html>
