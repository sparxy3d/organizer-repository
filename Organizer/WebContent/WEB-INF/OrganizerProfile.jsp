<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="head.inc.jsp" />
<body>
<style>
	h2{
		background-color:orange;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#fff;
		
		box-shadow: 1px 1px 3px #000;
	}
	h3{
		background-color:orange;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#fff;
		
		box-shadow: 1px 1px 3px #000;
	}
	#prof{
		background-color:#A457C2;
		margin:15px;
		padding:15px;
		text-align:center;
		color:#fff;
		font-size:1.2em;
		
		box-shadow: 2px 2px 5px #888;
	}
	span{
		font-size:1.3em;
		font-weight:bold;
	}
	span a{
		color:#fff;
	}
	span a:hover{
		text-decoration:none;
		color:#D4D4D4;
	}
	.blank{
		margin-top:50px;
	}
	img{
		margin: 10px auto;
	}
	#editprofile{
		border-radius:0px;
		box-shadow: 1px 1px 3px #000;
	}
	#editprofile a{
		color:#fff;
	}
	#editprofile a:hover{
		text-decoration:none;
		color:#fff;
	}
	.userEvents{
		background-color:#9C27B0;
		padding:15px;
		margin-bottom:15px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents p a{
		background-color:orange;
		color:#fff;
		padding:5px;
		
		box-shadow: 1px 1px 3px #000;
	}
	.userEvents p a:hover{
		text-decoration:none;
		color:#fff;
	}
	#currentevent h3{
		background-color:#E64A19;
	}
	
</style>
	<!--  header -->
	<jsp:include page="header.inc.jsp" />
	
	<div class="content">
		<div class="services-section1" id="services1">
			<div class="container">
				<jsp:include page="nav.inc.jsp" />			
			</div>
		</div>
		
		<div class="container">	
			
			<div class="col-md-12" id="prof">
				<div class="col-md-8 col-md-offset-2">
					<h2>${listUser.fname} ${listUser.lname}'s Profile</h2>				
				</div>
				<div class="col-md-8 col-md-offset-2" id="currentevent">
					<h3>Currently Organizer of ${listEvent.ename}</h3>				
				</div>
			
				<div class="col-md-8 col-md-offset-2">
					<img src="http://placehold.it/200" class="img-responsive">
				</div>
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<p>Hey. I'm <span>${listUser.fname} ${listUser.lname}</span> hailing from <span>${listUser.location}</span>.<br>
					I am a <span>${listUser.jobTitle }</span> at <span>${listUser.employer}</span>.<br/>
					You can get in touch with me <span><a href="mailto:${listUser.email}">here</a></span>. Cheers!!
					</p>
				</div>	
				
				<div class="col-sm-12 blank"></div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<h3>Past Events</h3>
					</div>
					<div class="col-sm-12 blank"></div>
					<c:forEach items="${listEvents}" var="event">					
					<div class="col-md-8 col-md-offset-2 userEvents">
						<p><span>${event.ename}</span> at <span>${event.venue}</span> on <span>${event.date}</span></p><br/>
						<p><a href="event?eid=${event.eid}">More details</a></p>
					</div>
					</c:forEach>
				</div>
				
				<div class="col-sm-12 blank"></div>
			</div>
			
		</div>
		
	</div>
	<jsp:include page="footer.inc.jsp" />
	
</body>
</html>